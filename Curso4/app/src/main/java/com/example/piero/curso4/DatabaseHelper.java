package com.example.piero.curso4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by PIERO on 20/12/2018.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME= "Curso.db";
    public static final String TABLE_NAME = "tablalogin";
    public static final String COL_1 = "ID";
    public static final String COL_2 = "NOMBRE";
    public static final String COL_3 = "APELLIDO";
    public static final String COL_4 = "CORREO";
    public static final String COL_5 = "CONTRASEÑA";


    public DatabaseHelper(Context context) {

        super(context, DATABASE_NAME, null, 1);
        //SQLiteDatabase db=this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table "+TABLE_NAME+"(ID INTEGER PRIMARY KEY AUTOINCREMENT, NOMBRE TEXT, APELLIDO TEXT, CORREO TEXT, CONTRASEÑA TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists "+TABLE_NAME);
        onCreate(db);
    }


    public boolean insertdata(String nom,String ape,String email,String pass){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(COL_2,nom);
        contentValues.put(COL_3,ape);
        contentValues.put(COL_4,email);
        contentValues.put(COL_5,pass);
        long result = db.insert(TABLE_NAME,null,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }
    //viendo si existe el email
    public Boolean checkmail(String email){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("Select * from "+TABLE_NAME+" where "+COL_4+"=?", new String[]{email});
        if(cursor.getCount()>0) return false;
        else return true;
    }

    public Boolean empass(String email,String pass){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor =db.rawQuery("Select * from "+TABLE_NAME+" where "+COL_4+"=? and "+COL_5+"=?",new String[]{email,pass});
        if(cursor.getCount()>0) return true;
        else return false;
    }
}
