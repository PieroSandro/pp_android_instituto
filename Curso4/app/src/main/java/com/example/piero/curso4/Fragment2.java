
package com.example.piero.curso4;



import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.nbsp.materialfilepicker.MaterialFilePicker;

import java.util.regex.Pattern;


public class Fragment2 extends Fragment {


    Button b1;

    public Fragment2(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_fragment2, container, false);

        b1=(Button)view.findViewById(R.id.button2);
        subirActivity();
        return view;

    }

    public void subirActivity(){
        b1.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent i=new Intent(getActivity(),uploadActivity.class);
                        startActivity(i);


                    }
                }
        );
    }
}
