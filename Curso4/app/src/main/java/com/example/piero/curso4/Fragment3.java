package com.example.piero.curso4;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class Fragment3 extends Fragment {
    Button b1;

    public Fragment3(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_fragment3, container, false);

        b1=(Button)view.findViewById(R.id.button4);
        bajarActivity();
        return view;

    }

    public void bajarActivity(){
        b1.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent i=new Intent(getActivity(),downloadActivity.class);
                        startActivity(i);


                    }
                }
        );
    }
}
