package com.example.piero.curso4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    EditText e1, e2;
    Button b1,b2;
    DatabaseHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mydb = new DatabaseHelper(this);
        e1 = (EditText) findViewById(R.id.editText);
        e2 = (EditText) findViewById(R.id.editText2);
        b1 = (Button) findViewById(R.id.button);
        b2 = (Button) findViewById(R.id.button2);
        /*b1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String email=e1.getText().toString();
                String passw=e2.getText().toString();
                Boolean checkmailpass=mydb.empass(email,passw);
                if(checkmailpass==true)
            }

        });*/
        ingresar();
        irRegistrarse();
    }


    public void ingresar() {
        b1.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email=e1.getText().toString();
                        String passw=e2.getText().toString();
                        Boolean checkmailpass=mydb.empass(email,passw);
                        if(checkmailpass==true) {

                            Toast.makeText(Login.this, "Bienvenido, ha ingresado exitosamente", Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Login.this, Menu2.class);//MainActivity2
                            startActivity(i);
                        }else
                            Toast.makeText(Login.this,"Datos equivocados, no puede ingresar",Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public void irRegistrarse(){
        b2.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent i=new Intent(Login.this,MainActivity.class);
                        startActivity(i);
                    }
                }
        );
    }

}