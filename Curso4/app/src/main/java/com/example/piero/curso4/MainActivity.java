package com.example.piero.curso4;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    RequestQueue rq;
    JsonRequest jrq;

    DatabaseHelper mydb;
    EditText e1,e2,e3,e4,e5;
    Button b1,b2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mydb=new DatabaseHelper(this);
        e1=(EditText)findViewById(R.id.nom);
        e2=(EditText)findViewById(R.id.ape);
        e3=(EditText)findViewById(R.id.email);
        e4=(EditText)findViewById(R.id.pass);
        e5=(EditText)findViewById(R.id.cpass);
        b1=(Button)findViewById(R.id.rbutton);
        b2=(Button)findViewById(R.id.rbutton2);
        agregar();
        rq = Volley.newRequestQueue(this);
        irlogin();
    }

    public void agregar(){
        b1.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        String s1 = e1.getText().toString();
                        String s2 = e2.getText().toString();
                        String s3 = e3.getText().toString();
                        String s4 = e4.getText().toString();
                        String s5 = e5.getText().toString();
                        if(s1.equals("")||s2.equals("")||s3.equals("")||s4.equals("")||s5.equals("")){
                            Toast.makeText(getApplicationContext(),"Deben llenarse todos los campos",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            if(s4.equals(s5)){
                                //SQLiteDatabase db=new SQLiteDatabase();
                                Boolean checkmail = mydb.checkmail(s3);
                                if(checkmail==true){
                                    boolean isInserted= mydb.insertdata(e1.getText().toString(),
                                            e2.getText().toString(),
                                            e3.getText().toString(),
                                            e4.getText().toString());
                                    if(isInserted=true) {
                                        Toast.makeText(MainActivity.this, "Datos registrados", Toast.LENGTH_LONG).show();
                                        registrarusuario();
                                    } else {
                                        Toast.makeText(MainActivity.this, "Datos no registrados", Toast.LENGTH_LONG).show();
                                    }}
                                else{
                                    Toast.makeText(MainActivity.this,"El correo ya existe",Toast.LENGTH_LONG).show();

                                }
                            }else{
                                Toast.makeText(MainActivity.this,"Confirme bien su contraseña",Toast.LENGTH_LONG).show();

                            }

                        }

                      /*boolean isInserted= mydb.insertdata(e1.getText().toString(),
                              e2.getText().toString(),
                              e3.getText().toString(),
                              e4.getText().toString());
                      if(isInserted=true)
                          Toast.makeText(MainActivity.this,"Datos registrados",Toast.LENGTH_LONG).show();
                      else
                          Toast.makeText(MainActivity.this,"Datos no registrados",Toast.LENGTH_LONG).show();*/

                    }
                }
        );
    }

    public void irlogin(){
        b2.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        Intent i=new Intent(MainActivity.this,Login.class);
                        startActivity(i);
                    }
                }
        );
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(MainActivity.this,"No se pudo registrar usuario "+error.toString(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {

        Toast.makeText(MainActivity.this,"Se pudo registrar "+e1.getText().toString(),Toast.LENGTH_SHORT).show();

        limpiar();
        /*JSONArray jsonArray=response.optJSONArray("datos");
        JSONObject jsonObject=null;

        try {
            jsonObject = jsonArray.getJSONObject(0);
            nombre.setId(jsonObject.optString("id"));
            nombre.setNombre(jsonObject.optString("nombre"));
            nombre.setApellido(jsonObject.optString("apellido"));
            nombre.setCorreo(jsonObject.optString("correo"));
            nombre.setContraseña(jsonObject.optString("contraseña"));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

    }

    void limpiar(){
        e1.setText("");
        e2.setText("");
        e3.setText("");
        e4.setText("");
        e5.setText("");
    }

    void registrarusuario(){
        String url="http://192.168.1.61/syncdemo/registrar.php?nombre="+e1.getText().toString()+
                "&apellido="+e2.getText().toString()+
                "&correo="+e3.getText().toString()+
                "&contraseña="+e4.getText().toString();
        jrq=new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        rq.add(jrq);
    }
}
