package com.example.piero.curso4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ImageButton boton1 =(ImageButton) findViewById(R.id.imageButton);
        boton1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Toast notification = Toast.makeText(MainActivity2.this,"My account",Toast.LENGTH_SHORT);
                notification.show();
                Menu2.option=1;
                Intent intent1= new Intent(getApplicationContext(),Menu2.class);
                startActivity(intent1);
            }
        });

        ImageButton boton2 =(ImageButton) findViewById(R.id.imageButton2);
        boton2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Toast notification = Toast.makeText(MainActivity2.this,"Upload service",Toast.LENGTH_SHORT);
                notification.show();
                Menu2.option=2;
                Intent intent2= new Intent(getApplicationContext(),Menu2.class);
                startActivity(intent2);
            }
        });

        ImageButton boton3 =(ImageButton) findViewById(R.id.imageButton3);
        boton3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Toast notification = Toast.makeText(MainActivity2.this,"Settings and tools",Toast.LENGTH_SHORT);
                notification.show();
                Menu2.option=3;
                Intent intent3= new Intent(getApplicationContext(),Menu2.class);
                startActivity(intent3);
            }
        });

        ImageButton boton4 =(ImageButton) findViewById(R.id.imageButton4);
        boton4.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Toast notification = Toast.makeText(MainActivity2.this,"Exit from the app",Toast.LENGTH_SHORT);
                notification.show();
                Menu2.option=4;
                Intent intent4= new Intent(getApplicationContext(),Menu2.class);
                startActivity(intent4);
            }
        });
    }

    @Override
    public void onClick(View view) {

    }
}
