package com.example.piero.curso4;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.session.MediaController;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.VideoView;

public class downloadActivity extends AppCompatActivity {

    long queueid;
    DownloadManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        BroadcastReceiver receiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action))
                {
                    DownloadManager.Query req_query = new DownloadManager.Query();
                    req_query.setFilterById(queueid);

                    Cursor c=dm.query(req_query);

                    if(c.moveToFirst())
                    {
                        int columnIndex=c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if(DownloadManager.STATUS_SUCCESSFUL==c.getInt(columnIndex)){
                            /*VideoView videoView = (VideoView)findViewById(R.id.videoView);
                            String uriString=c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));

                            MediaController mediaController = new MediaController(getApplicationContext());
                            mediaController.setAnchorView(videoView);
                            videoView.requestFocus();
                            videoView.setVideoURI(Uri.parse(uriString));
                            videoView.start();*/

                        }
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));


    }

    public void Download_Click(View v)
    {
        dm=(DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse("https://drive.google.com/drive/folders/0B6UdvLYGL-DJWGVwaTNYOWxLS1E?fbclid=IwAR2f8FCa_UyxOdWk9bRsxRxmJuYlOHcxrpaQNSMLKp4aFMBDvny8sR2Zh3U"));

        queueid=dm.enqueue(request);

    }

    public void View_Click(View v){
        Intent i=new Intent();
        i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
        startActivity(i);

    }
}
