-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-01-2019 a las 21:22:24
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cursodb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `text` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `text`) VALUES
(1, 'Administración'),
(2, 'Computación'),
(3, 'Contabilidad'),
(4, 'Electrónica'),
(5, 'Electrotecnia'),
(6, 'Laboratorio Clínico'),
(7, 'Mecánica Automotriz'),
(8, 'Mecánica de Producción'),
(9, 'Metalurgia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curso`
--

CREATE TABLE `curso` (
  `id_curso` int(4) NOT NULL,
  `nombre_curso` varchar(25) DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `curso`
--

INSERT INTO `curso` (`id_curso`, `nombre_curso`, `costo`, `imagen`, `descripcion`) VALUES
(1, 'Ingles elemental', 250, 'images/alfabeto.png', 'Dirigido a estudiantes sin conocimiento alguno del idioma'),
(3, 'Ingles intermedio', 300, 'images/interenglish.jpg', 'Dirigido a estudiantes con conocimientos de gramatica y que dominan una comunicacion fluida'),
(4, 'Ingles avanzado', 600, 'images/.', 'Dirigido a estudiantes con conocimientos completos de gramatica, estilos de redaccion y lectura'),
(5, 'Ingles Profesional', 350, 'images/classmates.jpg', 'para el Ã¡mbito academico...'),
(6, 'ingles senati', 650, 'images/.', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_bin NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `event`
--

INSERT INTO `event` (`id`, `title`, `start`, `end`) VALUES
(1, 'event1', '2018-09-25 00:00:00', '2018-09-26 00:00:00'),
(2, 'event2', '2018-09-28 00:00:00', '2018-09-29 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE `evento` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `creado` datetime NOT NULL,
  `modificado` datetime NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`id`, `titulo`, `fecha`, `creado`, `modificado`, `estado`) VALUES
(1, 'PHP : Today PHP Event Calendar Class', '2017-04-22', '2017-04-22 06:15:17', '2017-04-22 06:15:17', 1),
(2, 'Laravel: Laravel - The PHP Framework Class', '2017-04-10', '2017-04-10 06:15:17', '2017-04-10 06:15:17', 1),
(3, 'javascript onchange event example class', '2018-09-24', '2018-09-24 06:15:17', '2018-09-24 06:15:17', 1),
(4, 'mailchimp integration in php', '2018-09-26', '2018-09-26 06:15:17', '2018-09-26 06:15:17', 1),
(5, 'api laravel tutorial', '2018-09-29', '2018-09-29 06:15:17', '2018-09-29 06:15:17', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento2`
--

CREATE TABLE `evento2` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_bin NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `evento2`
--

INSERT INTO `evento2` (`id`, `titulo`, `fecha_inicio`, `fecha_fin`) VALUES
(1, 'evento1', '2019-01-16 00:00:00', '2019-01-23 00:00:00'),
(2, 'evento2', '2018-09-28 00:00:00', '2018-09-29 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Block'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `title`, `date`, `created`, `modified`, `status`) VALUES
(6, 'Examen', '2019-01-16 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(8, 'examen', '2018-09-29 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(10, 'examen', '2018-09-30 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(11, 'admision', '2018-10-04 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(12, 'titulacion', '2018-10-06 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(13, 'repaso', '2018-09-24 00:00:00', '2018-10-23 07:16:23', '0000-00-00 00:00:00', 1),
(14, 'prueba unitaria', '2018-10-07 22:10:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(15, 'prueba integral', '2018-10-09 07:30:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(16, 'title33', '2018-10-07 22:30:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(17, 'ooo555', '2018-10-07 22:35:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(18, 'olo', '2018-10-07 23:35:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(19, 'madruga', '2018-10-08 03:30:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(20, 'coco', '2018-10-08 03:35:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(21, 'juego', '2018-10-11 04:20:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(22, 'árbol', '2018-10-16 03:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(23, 'españa esperanza', '2018-10-17 10:30:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events2`
--

CREATE TABLE `events2` (
  `id` int(11) NOT NULL,
  `title` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `events2`
--

INSERT INTO `events2` (`id`, `title`, `date`) VALUES
(1, '', '0000-00-00 00:00:00'),
(2, 'almorzón', '2018-10-23 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foo`
--

CREATE TABLE `foo` (
  `creation_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `modification_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `foo`
--

INSERT INTO `foo` (`creation_time`, `modification_time`) VALUES
('2018-10-08 18:09:57', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `category` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `body` longtext NOT NULL,
  `author` varchar(100) NOT NULL,
  `keywords` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `posts`
--

INSERT INTO `posts` (`id`, `title`, `category`, `date`, `body`, `author`, `keywords`) VALUES
(1, 'php post', 2, '0000-00-00 00:00:00', '<p>This blog post shows a few different types of content that''s supported and styled with Bootstrap. Basic typography, images, and code are all supported.</p>\r\n            <hr>\r\n            <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>\r\n            <blockquote>\r\n              <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\r\n            </blockquote>\r\n            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>\r\n            <h2>Heading</h2>\r\n            <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>\r\n            <h3>Sub-heading</h3>\r\n            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n            <pre><code>Example code block</code></pre>\r\n            <p>Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</p>\r\n            <h3>Sub-heading</h3>\r\n            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>\r\n            <ul>\r\n              <li>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</li>\r\n              <li>Donec id elit non mi porta gravida at eget metus.</li>\r\n              <li>Nulla vitae elit libero, a pharetra augue.</li>\r\n            </ul>\r\n            <p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue.</p>\r\n            <ol>\r\n              <li>Vestibulum id ligula porta felis euismod semper.</li>\r\n              <li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>\r\n              <li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li>\r\n            </ol>\r\n            <p>Cras mattis consectetur purus sit amet fermentum. Sed posuere consectetur est at lobortis.</p>\r\n            ', 'Daniel', 'php'),
(2, 'Java post', 1, '0000-00-00 00:00:00', '<p>This blog post shows a few different types of content that''s supported and styled with Bootstrap. Basic typography, images, and code are all supported.</p>\r\n            <hr>\r\n            <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>\r\n            <blockquote>\r\n              <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>\r\n            </blockquote>\r\n            <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>\r\n            <h2>Heading</h2>\r\n            <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>\r\n            <h3>Sub-heading</h3>\r\n            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>\r\n            <pre><code>Example code block</code></pre>\r\n            <p>Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</p>\r\n            <h3>Sub-heading</h3>\r\n            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>\r\n            <ul>\r\n              <li>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</li>\r\n              <li>Donec id elit non mi porta gravida at eget metus.</li>\r\n              <li>Nulla vitae elit libero, a pharetra augue.</li>\r\n            </ul>\r\n            <p>Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue.</p>\r\n            <ol>\r\n              <li>Vestibulum id ligula porta felis euismod semper.</li>\r\n              <li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</li>\r\n              <li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li>\r\n            </ol>\r\n            <p>Cras mattis consectetur purus sit amet fermentum. Sed posuere consectetur est at lobortis.</p>\r\n            ', 'Julio', 'java'),
(3, '4 efectos saludables de aprender idiomas', 0, '2018-10-08 18:15:41', 'Aprender un segundo idioma no sólo te resulta útil para acceder a un puesto de trabajo o viajar por el extranjero. Además tiene poderosos efectos sobre el cerebro, según demuestran recientes estudios científicos.\r\n\r\nPlasticidad cerebral. Tras examinar a 105 personas de las que 80 eran bilingües, científicos del University College de Londres (Reino Unido) detectaron que conocer un segundo idioma modifica de manera positiva la estructura del cerebro, en concreto el área que procesa información. En particular, mejora la llamada plasticidad cerebral, potenciando el aprendizaje y la memoria. \r\n\r\nRetrasa el Alzheimer. Ellen Bialystok, profesora de Psicología de la Universidad de York en Toronto (Canadá), realizó un estudio con 450 pacientes con Alzheimer, la mitad de los cuáles había hablado dos lenguas la mayor parte de su vida, mientras el resto sólo manejaba una. Bialystok encontró que las personas que hablaban más de un idioma empezaron a mostrar los síntomas de la enfermedad entre 4 y 5 años más tarde.\r\n\r\nMás concentrados. De acuerdo con una investigación publicada el año pasado en la revista Psychological Science, los niños que aprenden más de un idioma tienen más capacidad de concentrarse y focalizar su atención, ignorando las interferencias que pueden distraerlos. \r\n\r\nGimnasia mental. Cuando una persona bilingüe cambia de un idioma a otro está ejercitando su cerebro, según ha podido comprobar Judith Kroll, del Centro de Ciencia del Lenguaje de la Universidad Penn State (EE UU). Esta "gimnasia cerebral" le permite manejarse mejor en situaciones de multitarea, es decir, trabajar en varios proyectos al mismo tiempo.\r\n\r\nfuente: https://www.muyinteresante.es/salud/articulo/4-efectos-saludables-de-aprender-idiomas', 'Elena Sanz', 'alzheimer memoria'),
(4, 'Drops te ayuda a aprender idiomas con imágenes y 5 minutos al día', 0, '2018-10-08 18:31:16', 'Cada nueva app que sale al mercado sorprende con nuevas propuestas. Un ejemplo es Drops, que permite aprender idiomas a través de imágenes en cuestión de minutos.\r\n\r\nCuando nos enseñan a leer de pequeños, normalmente los libros o cuadernos con palabras y letras suelen venir decorados con imágenes que nos ayudan a asociar conceptos, palabras o grafías con un animal u objeto familiar.\r\n\r\nEl método no es infalible pero sí tiene su razón de ser, y es que, muchos recordamos mejor imágenes que conceptos abstractos en forma de texto. Así, la palabra gato asociada al dibujo de un gato, se nos quedará grabada en la mente con más facilidad que la palabra en sí.\r\n\r\nDe adultos esto también funciona de este modo. De ahí que, por ejemplo, en plazas de aparcamiento de grandes superficies comerciales, junto con el número de plaza y la planta, se incluya un dibujo. Tal vez olvides el número, pero recordarás la imagen.\r\n\r\nPrecisamente, por eso Drops apuesta por un método clásico de enseñanza para aprender idiomas mediante imágenes.\r\n\r\nPara impacientes\r\nLa aplicación oficial de Drops está disponible en Android e iPhone/iPad y, en el momento de escribir estas líneas, nos permite aprender 29 idiomas diferentes.\r\n\r\nEn concreto, los idiomas disponibles son coreano, japonés, chino mandarín y cantonés, castellano (de España e Hispanoamérica), francés, alemán, holandés, italiano, ruso, portugués (europeo y brasileño), hebreo, árabe, turco, tagalog, vietnamita, indonesio, danés, sueco, noruego, islandés, húngaro, esperanto, hindi y, cómo no, inglés (americano y británico).\r\n\r\nMayor informacion en https://blogthinkbig.com/drops-te-ayuda-a-aprender-idiomas-con-imagenes-y-5-minutos-al-dia', 'José María López', 'drops app'),
(5, 'Diccionarios online para entender expresiones en inglés de internet', 0, '2018-10-08 18:31:16', 'La lengua está viva y evoluciona a pasos agigantados, en especial en entornos tan cambiantes como internet. Si no quieres perderte con las expresiones en inglés y siglas más modernas, te recomendamos recursos donde encontrar su significado.\r\n\r\nInternet ha cambiado muchas cosas en el día a día. Ha cambiado la manera de comprar, de trabajar, de comunicarnos y ha cambiado incluso el idioma. Si los SMS cambiaron nuestra manera de escribir, y aún ahora con WhatsApp hay quien escribe “xq” en vez de “por que” o “dnd” en vez de “donde”, internet ha facilitado el intercambio de palabras y expresiones propias de diferentes regiones y países, acuñando también expresiones en inglés que dan la vuelta por todo el mundo.\r\n\r\nPuede que te suenen las siglas WTF, LOL o BFF, o es posible que en el trabajo alguien haya escrito ASAP o IMHO en algún correo electrónico. Cada día surgen nuevas palabras, siglas o expresiones en inglés que superan a las anteriores y que, si bien facilitan y enriquecen la comunicación, si no las conoces pueden hacer que te pierdas en una conversación o no entiendas lo que lees en un blog, en YouTube o en los comentarios de Instagram.\r\n\r\nCada vez hay una distancia más grande entre el inglés que aprendemos en una academia, o en el instituto, y el inglés que se habla en la calle o en internet, en especial por los más jóvenes. Ocurre en todos los idiomas, también el español, pero como el inglés está en prácticamente todos los rincones de la red y es muy maleable, es la lengua con una jerga más rica e ininteligible.\r\n\r\nA continuación te ofrecemos algunas fuentes o diccionarios online que te ayudarán a resolver tus dudas y a conocer las expresiones en inglés más populares, el slang o jerga que se utiliza en internet.\r\n\r\nUrban Dictionary\r\nUno de los primeros diccionarios online especializados en slang de internet es Urban Dictionary. Ahí encontrarás prácticamente cualquier expresión en inglés que hayas encontrado en un foro de internet o en los comentarios de un vídeo. Puedes buscar por palabra clave o mirar letra a letra.\r\n\r\nCada entrada ofrece una definición, palabras relacionadas y la posibilidad de compartirla en redes sociales. Otro detalle interesante es su tienda online, con merchandising personalizable con las expresiones que te parezcan más divertidas. Tazas, camisetas e incluso un juego de preguntas para comprobar quién sabe más jerga de internet. Si bien nació en la web, ahora mismo dispone también de aplicación para Android y iPhone/iPad. Así podrás resolver tu duda estés donde estés.\r\n\r\nInternet Slang\r\nOtra fuente útil para conocer la definición y significado de expresiones y siglas en inglés es Internet Slang. Puedes buscar a partir de la lista de expresiones más utilizadas, letra a letra o ver palabras al azar para aprender las más nuevas o algunas que desconozcas.\r\n\r\nThe Online Sang Dictionary\r\nCon un diseño más sobrio pero mejor estructurado, The Online Slang Dictionary es un diccionario de expresiones en inglés que te permite encontrar palabras en su buscador, letra a letra y descubrir nuevas con la palabra del día, las definiciones recién añadidas y la lista de palabras y expresiones más populares.\r\n\r\nComo en el caso de Urban Dictionary, ofrece frases de ejemplo y definiciones.\r\n\r\nSayWhat\r\nSayWhat no es exactamente un diccionario de expresiones slang. Se trata de un repositorio de vídeos donde aprender cómo se pronuncian ciertas palabras y expresiones y su significado. Cuenta con buscador y con un montón de categorías, entre las que encontrarás slang con expresiones modernas de distintas regiones: Reino Unido, Australia, Sudáfrica, Canadá e incluso China. Así conocerás las expresiones típicas de cada lugar y diferentes maneras de nombrar algo en inglés según dónde te encuentres.\r\n\r\nAdemás de la versión web, SayWhat permite descargar su app para Androidy iPhone/iPad donde encontrarás los mismos vídeos y podrás colaborar con tus aportaciones.\r\n\r\nSlang Dictionary\r\nEn este práctico diccionario online también podrás resolver tus dudas sobre jergas del inglés, americanas, inglesas o australianas. Como en los diccionarios anteriores, Slang Dictionary tiene buscador, lista de palabras por letra, palabra del día y las palabras más buscadas, para hacerte una idea de las más populares. Mayores detalles en https://blogthinkbig.com/diccionarios-online-para-entender-expresiones-en-ingles-de-internet', 'See-ming Lee', 'slang videos'),
(6, 'Pronunciación inglesa vs. pronunciación americana', 0, '2018-10-08 18:36:09', 'Siempre nos han interesado las diferencias y similitudes entre el inglés británico y el inglés de Norteamérica. A continuación vamos a ver las seis diferencias más importantes en cuanto a pronunciación se refiere a la hora de aprender estas dos variantes del inglés. ¡Y con audios! Abre los oídos, que allá vamos.\r\n\r\nMayor información: https://grupovaughan.com/a/pronunciacion-inglesa-vs-pronunciacion-americana/', 'Vaughan', 'nivel básico, inglés británico'),
(7, 'fg', 3, '2019-01-09 11:23:45', 'rerereeeeeereererere', 'yo', 'nol');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pregunta`
--

CREATE TABLE `pregunta` (
  `preg_id` int(5) NOT NULL,
  `n_pregunta` varchar(250) DEFAULT NULL,
  `resp_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pregunta`
--

INSERT INTO `pregunta` (`preg_id`, `n_pregunta`, `resp_id`) VALUES
(1, 'Para que sirve?', 1),
(2, 'Como es?', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuesta`
--

CREATE TABLE `respuesta` (
  `r_id` int(5) NOT NULL,
  `n_respuesta` varchar(250) DEFAULT NULL,
  `resp_id` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `respuesta`
--

INSERT INTO `respuesta` (`r_id`, `n_respuesta`, `resp_id`) VALUES
(1, 'casa', 1),
(2, 'bebe', 1),
(3, 'ggggg', 1),
(4, 'fbfgfgg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `useremail`
--

CREATE TABLE `useremail` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `correo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `useremail`
--

INSERT INTO `useremail` (`id`, `nombre`, `apellido`, `telefono`, `correo`, `mensaje`) VALUES
(1, 'yo', 'tu', '8888', 'p@gmail.com', 'wawawawawawawwawawyayayyayayyayayyya'),
(2, 'a', 'bi', '3333', 'lo@gmail.com', 'blabla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `password` varchar(200) CHARACTER SET utf8 NOT NULL,
  `session` varchar(100) CHARACTER SET utf8 NOT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(15) NOT NULL,
  `nombre_usuario` varchar(25) DEFAULT NULL,
  `especialidad` varchar(25) DEFAULT NULL,
  `turno` varchar(25) DEFAULT NULL,
  `ciclo` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`id_curso`);

--
-- Indices de la tabla `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `evento2`
--
ALTER TABLE `evento2`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events2`
--
ALTER TABLE `events2`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  ADD PRIMARY KEY (`preg_id`);

--
-- Indices de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  ADD PRIMARY KEY (`r_id`);

--
-- Indices de la tabla `useremail`
--
ALTER TABLE `useremail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `curso`
--
ALTER TABLE `curso`
  MODIFY `id_curso` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `evento`
--
ALTER TABLE `evento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `evento2`
--
ALTER TABLE `evento2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `events2`
--
ALTER TABLE `events2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `pregunta`
--
ALTER TABLE `pregunta`
  MODIFY `preg_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `respuesta`
--
ALTER TABLE `respuesta`
  MODIFY `r_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `useremail`
--
ALTER TABLE `useremail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(15) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
